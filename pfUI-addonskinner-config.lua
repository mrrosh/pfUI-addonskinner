--                MODULE        SUBGROUP       ENTRY               VALUE
-- pfUI:UpdateConfig("module_template", nil, nil, 0)

-- load pfUI environment
setfenv(1, pfUI:GetEnvironment())

function pfUI:LoadConfig_addonskinner()
  pfUI:UpdateConfig("addonskinner", nil,         "notifications",    "0")
  pfUI:UpdateConfig("addonskinner", "disabled",  nil,                nil)
end
