This addon is an external module for [pfUI](https://gitlab.com/shagu/pfUI) addon.

## Screenshots
![settings](settings.png)


## Description
Addon provides you with pfUI-themed skins for other addons. It also allowse you to create and load your own skins.

You can check some code in `skins` folder to get the idea of how it is done and go on from there with your own skins and ideas.
Do not forget to add your skins to `pfUI-addonskinner.toc` file.

**Please, keep in mind that I won't skin addons that I personally don't use and therefore won't add them to this repository. Feel free to fork and publish your personal skinns on your own. Thanks for understanding.**


## Installation
**This addon will not function without [pfUI](https://gitlab.com/shagu/pfUI) installed**
1. Download **[Latest Version](https://gitlab.com/dein0s_wow_vanilla/pfUI-addonskinner/-/archive/master/pfUI-master.zip)**
2. Unpack the Zip file
3. Rename the folder to "pfUI-addonskinner"
4. Copy "pfUI-addonskinner" into Wow-Directory\Interface\AddOns
5. Restart WoW


## Credits
Thanks to [RoadBlock](https://github.com/Road-block/) for sharing his original idea, code and a bunch of skins.
I probably wouldn't beautify and publish my own stuff otherwise )
