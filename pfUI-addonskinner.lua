pfUI:RegisterModule("addonskinner", function ()

  pfUI:LoadConfig_addonskinner()
  pfUI:LoadGui_addonskinner()

  -- if C.addonskinner == nil then
  --   C.addonskinner = {}
  -- end

  local msg_pref = "|cff33ffccpf|rUI Addon Skinner|r: "

  pfUI.addonskinner_skin = {}
  pfUI.addonskinner_list = {}

  local config = C.addonskinner

  pfUI.addonskinner = CreateFrame("Frame", "pfAddonSkinner", UIParent)
  pfUI.addonskinner:RegisterEvent("PLAYER_LOGIN")

  function pfUI.addonskinner:RegisterSkin(addon_name, skin_function)
    if pfUI.addonskinner_skin[addon_name] then return end
    pfUI.addonskinner_skin[addon_name] = skin_function
    table.insert(pfUI.addonskinner_list, addon_name)
  end

  function pfUI.addonskinner:UnregisterSkin(addon_name)
    if pfUI.addonskinner_skin[addon_name] then
      pfUI.addonskinner_skin[addon_name] = nil
    end
  end

  function pfUI.addonskinner:Load()
    -- purge non-existent skins from config
    for addon_name in config.disabled do
      if pfUI.addonskinner_skin[addon_name] == nil then
        config.disabled[addon_name] = nil
      end
    end

    -- load skins
    for i, addon_name in pairs(pfUI.addonskinner_list) do
      if IsAddOnLoaded(addon_name) then
        if config.disabled[addon_name] ~= "1" then
          pfUI.addonskinner_skin[addon_name]()
          if config.notifications == "1" then
            DEFAULT_CHAT_FRAME:AddMessage(msg_pref .. addon_name .. " skinned")
          end
        end
      end
    end
  end

  pfUI.addonskinner:SetScript("OnEvent", pfUI.addonskinner.Load)

  function pfUI.addonskinner:Update()
  end

end)
