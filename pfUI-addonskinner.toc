## Interface: 11200
## Title: |cff33ffccpf|cffffffffUI - addonskinner
## Author: dein0s and RoadBlock
## Notes: External module for |cff33ffccpf|cffffffffUI
## Version: 0.2
## Dependencies: pfUI

# main
pfUI-addonskinner-config.lua
pfUI-addonskinner-gui.lua
pfUI-addonskinner.lua
pfUI-addonskinner-api.lua

# skins
skins\BigWigs.lua
skins\ItemHints.lua
skins\ItemRack.lua
skins\LoseControl.lua
skins\RABuffs.lua
skins\RingMenu.lua
skins\shootyepgp.lua
skins\SP_Overpower.lua
skins\SP_SwingTimer.lua
skins\TrinketMenu.lua
skins\SW_Stats.lua
skins\ignitestatus.lua
skins\ModifiedPowerAuras.lua
