pfUI.addonskinner:RegisterSkin("RingMenu", function()
  for i=1, RingMenu_settings.numButtons do
    local btn = getglobal("RingMenuButton" .. i)
    pfUI.addonskinner.api.SkinActionButton(btn)
  end

  pfUI.addonskinner:UnregisterSkin("RingMenu")

end)