pfUI.addonskinner:RegisterSkin("SP_SwingTimer", function()
  pfUI.api.CreateBackdrop(SP_ST_Frame)
  local font_string = getglobal("SP_ST_FrameText")
  font_string:SetFont(pfUI_config.global.font_combat, 14, "NORMAL")

  pfUI.addonskinner:UnregisterSkin("SP_SwingTimer")
end)